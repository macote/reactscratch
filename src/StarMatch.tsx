import React, { useState, useEffect, useRef } from "react";
import { flushSync } from "react-dom";
import './StarMatch.css';

enum PlayNumberStatus {
  Available,
  Used,
  Wrong,
  Candidate,
}

const tsLog = (data: string) => {
  console.log(`[${new Date().toISOString()}] ${data}`);
}

const statusColor = (status: PlayNumberStatus) => {
  switch (status) {
    case PlayNumberStatus.Available:
      return 'lightgray';
    case PlayNumberStatus.Used:
      return 'lightgreen';
    case PlayNumberStatus.Wrong:
      return 'lightcoral';
    case PlayNumberStatus.Candidate:
      return 'deepskyblue';
    default:
      break;
  }
};

const StarsDisplay = (props: { count: number }) => (
  <>
    {utils.range(1, props.count).map(starId => (
      <div key={starId} className="star" />
    ))}
  </>
);

const PlayNumber = (props: { status: PlayNumberStatus, number: number, onClick: (number: number, status: PlayNumberStatus) => void }) => {
  return (
    <button
      className="number"
      style={{ backgroundColor: statusColor(props.status) }}
      onClick={() => props.onClick(props.number, props.status)}
    >
      {props.number}
    </button>
  );
}

const PlayAgain = (props: { gameStatus: string, onClick: () => void }) => (
  <div className="game-done">
    <div
      className="message"
      style={{ color: props.gameStatus === 'lost' ? 'red' : 'green' }}
    >
      {props.gameStatus === 'lost' ? 'Game Over' : 'Nice'}
    </div>
    <button onClick={props.onClick}>Play Again</button>
  </div>
);

const useGameState = () => {
  const [stars, setStars] = useState(utils.random(1, 9));
  const [availableNums, setAvailableNums] = useState(utils.range(1, 9));
  const [candidateNums, setCandidateNums] = useState<number[]>([]);
  const [secondsLeft, setSecondsLeft] = useState(10);
  const intervalIdRef = useRef(0);

  useEffect(() => {
    tsLog(`[R] secondsLeft: ${secondsLeft}; availableNumCount: ${availableNums.length}`);
    if (secondsLeft > 0 && availableNums.length > 0) {
      if (intervalIdRef.current === 0) {
        intervalIdRef.current = setInterval(() => {
          tsLog(`[I] setInterval(${intervalIdRef.current});`);
          let setterExecuted = false; // flag to check if component is still mounted...
          setSecondsLeft(msl => {
            tsLog(`[I] secondsLeft: ${msl}`);
            setterExecuted = true;
            return msl - 1; 
          });
          setTimeout(() => {
            if (!setterExecuted) {
              tsLog(`[I] clearInterval(${intervalIdRef.current}); secondsLeft: ${secondsLeft}`);
              clearInterval(intervalIdRef.current);            
            }
          }, 333);
          // flushSync(() => {
          //   setSecondsLeft(msl => {
          //     tsLog(`[I] secondsLeft: ${msl}`);
          //     setterExecuted = true;

          //     return msl - 1;
          //   });
          // });
          // if (!setterExecuted) {
          //   tsLog(`[I] clearInterval(${intervalIdRef.current}); secondsLeft: ${secondsLeft}`);
          //   clearInterval(intervalIdRef.current);
          // }
        }, 1000, null);
      }

      return () => {
        tsLog(`[U] secondsLeft: ${secondsLeft}; availableNumCount: ${availableNums.length}`);
        if (/* clean-up was not triggered by a re-render... */ secondsLeft === secondsLeft + 1) {
          tsLog(`[U] clearInterval(${intervalIdRef.current}); secondsLeft: ${secondsLeft}`);
          clearInterval(intervalIdRef.current);
        }
      }
    } else {
      tsLog(`[R] clearInterval(${intervalIdRef.current}); secondsLeft: ${secondsLeft}`);
      clearInterval(intervalIdRef.current);
      intervalIdRef.current = 0;
    }
  });

  const setGameState = (newCandidateNums: number[]) => {
    if (utils.sum(newCandidateNums) !== stars) {
      setCandidateNums(newCandidateNums);
    } else {
      const newAvailableNums = availableNums.filter(
        n => !newCandidateNums.includes(n)
      );
      setStars(utils.randomSumIn(newAvailableNums, 9));
      setAvailableNums(newAvailableNums);
      setCandidateNums([]);
    }
  };

  return { stars, availableNums, candidateNums, secondsLeft, setGameState };
};

const Game = (props: { startNewGame: () => void }) => {
  const {
    stars,
    availableNums,
    candidateNums,
    secondsLeft,
    setGameState,
  } = useGameState();

  const candidatesAreWrong = utils.sum(candidateNums) > stars;

  const gameStatus = availableNums.length === 0
    ? 'won'
    : secondsLeft === 0 ? 'lost' : 'active'

  const numberStatus = (number: number): PlayNumberStatus => {
    if (!availableNums.includes(number)) {
      return PlayNumberStatus.Used;
    }
    if (candidateNums.includes(number)) {
      return candidatesAreWrong ? PlayNumberStatus.Wrong : PlayNumberStatus.Candidate;
    }

    return PlayNumberStatus.Available;
  };

  const onNumberClick = (number: number, currentStatus: PlayNumberStatus) => {
    if (gameStatus !== 'active' || currentStatus === PlayNumberStatus.Used) {
      return;
    }

    const newCandidateNums =
      currentStatus === PlayNumberStatus.Available
        ? candidateNums.concat(number)
        : candidateNums.filter(cn => cn !== number);

    setGameState(newCandidateNums);
  };

  return (
    <div className="game">
      <div className="help">
        Pick 1 or more numbers that sum to the number of stars
      </div>
      <div className="body">
        <div className="left">
          {gameStatus !== 'active' ? (
            <PlayAgain onClick={props.startNewGame} gameStatus={gameStatus} />
          ) : (
            <StarsDisplay count={stars} />
          )}
        </div>
        <div className="right">
          {utils.range(1, 9).map(number => (
            <PlayNumber
              key={number}
              status={numberStatus(number)}
              number={number}
              onClick={onNumberClick}
            />
          ))}
        </div>
      </div>
      <div className="timer">Time Remaining: {secondsLeft}</div>
    </div>
  );
};

const StarMatch = () => {
  const [gameId, setGameId] = useState(1);
  const [show, setShow] = useState(true);
  return (
    <>
      <div className="game">
        <button onClick={() => setGameId(gameId + 1)}>Reset</button>
        <button onClick={() => setShow(!show)}>{show ? 'Hide' : 'Show'}</button>
      </div>
      {show && <Game key={gameId} startNewGame={() => setGameId(gameId + 1)} />}
    </>
  )
}

// Math science
const utils = {
  // Sum an array
  sum: (arr: number[]) => arr.reduce((acc, curr) => acc + curr, 0),

  // create an array of numbers between min and max (edges included)
  range: (min: number, max: number) => Array.from({ length: max - min + 1 }, (_, i) => min + i),

  // pick a random number between min and max (edges included)
  random: (min: number, max: number) => min + Math.floor(Math.random() * (max - min + 1)),

  // Given an array of numbers and a max...
  // Pick a random sum (< max) from the set of all available sums in arr
  randomSumIn: (arr: number[], max: number) => {
    const sets: number[][] = [[]];
    const sums = [];
    for (let i = 0; i < arr.length; i++) {
      for (let j = 0, len = sets.length; j < len; j++) {
        const candidateSet = sets[j].concat(arr[i]);
        const candidateSum = utils.sum(candidateSet);
        if (candidateSum <= max) {
          sets.push(candidateSet);
          sums.push(candidateSum);
        }
      }
    }
    return sums[utils.random(0, sums.length - 1)];
  },
};

export default StarMatch;