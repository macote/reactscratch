import React from "react";
import { useRef, useState } from "react";

export default function Form() {
  const [answer, setAnswer] = useState("");
  const [error, setError] = useState<unknown>(null);
  const [status, setStatus] = useState('typing');
  const formRef = useRef<HTMLFormElement>(null);

  if (status === 'success') {
    return <h1>That&apos;s right!</h1>
  }

  // const errorMessage = (error as Error).message;

  async function handleSubmit(e: React.SyntheticEvent) {
    e.preventDefault();
    setStatus('submitting');
    try {
      // const target = e.target as typeof e.target & {
      //   city: { value: string };
      // };
      // const city = target.city.value; // typechecks!
      // const form = e.target as HTMLFormElement;
      // form.submit();
      setStatus('success');
    } catch (err) {
      setStatus('typing');
      setError(err);
    }
  }

  function handleTextareaChange(e: React.ChangeEvent<HTMLTextAreaElement>) {
    setAnswer(e.target.value);
  }

  return (
    <>
      <h2>City quiz</h2>
      <p>
        In which city is there a billboard that turns air into drinkable water?
      </p>
      <form ref={formRef} onSubmit={handleSubmit}>
        <textarea
          name='city'
          value={answer}
          onChange={handleTextareaChange}
          disabled={status === 'submitting'}
        />
        <br />
        <button disabled={
          answer.length === 0 ||
          status === 'submitting'
        }>
          Submit
        </button>
        {error !== null &&
          <p className="Error">
            {`${error}`}
          </p>
        }
      </form>
    </>
  );
}
