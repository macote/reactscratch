/* eslint-disable @typescript-eslint/no-unused-vars */

import React from 'react';
import './App.css';
import FilterableProductTable from './FilterableProductTable';
import Form from './Form';
import { Product } from './model';
import StarMatch from './StarMatch';

const PRODUCTS: Product[] = [
  { category: "Fruits", price: "$1", stocked: true, name: "Apple" },
  { category: "Fruits", price: "$1", stocked: true, name: "Dragonfruit" },
  { category: "Fruits", price: "$2", stocked: false, name: "Passionfruit" },
  { category: "Vegetables", price: "$2", stocked: true, name: "Spinach" },
  { category: "Vegetables", price: "$4", stocked: false, name: "Pumpkin" },
  { category: "Vegetables", price: "$1", stocked: true, name: "Peas" }
];

function App() {
  return (
    <>
      {/* <FilterableProductTable products={PRODUCTS} />
      <Form />  */}
      <StarMatch />
    </>
  );
}

export default App;
